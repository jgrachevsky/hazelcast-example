package com.example.hazelcast

import com.hazelcast.core.HazelcastInstance
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class HazelcastApplicationTests {

    @Autowired
    private lateinit var hazelcastInstance: HazelcastInstance

    @Test
    fun contextLoads() {
        val semaphore = hazelcastInstance.getSemaphore("semaphore")

        hazelcastInstance.distributedObjects.find { it.name == "semaphore" }

//        println(hazelcastInstance.distributedObjects.find { it.name == "test" }?.let {
//            println("Service: ${it.serviceName}")
//            println("Name: ${it.name}")
//        })
//        semaphore.init(2)
//        println("Inited")
        println("Available permits: ${semaphore.availablePermits()}")
        println("Drain permits: ${semaphore.drainPermits()}")

//        semaphore.acquire()
//        println("Acquired")
//        println("Available permits: ${semaphore.availablePermits()}")
//        println("Drain permits: ${semaphore.drainPermits()}")

//        semaphore.release()
//        println("Released")
//        println("Available permits: ${semaphore.availablePermits()}")
//        println("Drain permits: ${semaphore.drainPermits()}")

//        semaphore.acquire()
//        println("Acquired")
//        println("Available permits: ${semaphore.availablePermits()}")
//        println("Drain permits: ${semaphore.drainPermits()}")

//        semaphore.init(5)
//        println("Re-inited")
//        println("Available permits: ${semaphore.availablePermits()}")
//        println("Drain permits: ${semaphore.drainPermits()}")
    }

}

