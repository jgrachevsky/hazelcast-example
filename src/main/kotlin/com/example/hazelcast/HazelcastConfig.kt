/*
 *  Created by Yakov Grachevsky
 *          at 1/27/19
 * -------------------------------------
 *    Copyright (c) 2019, BD Innovations (https://bd-innovations.com/) All Rights Reserved 
 */

package com.example.hazelcast

import com.hazelcast.config.Config
import com.hazelcast.config.EvictionPolicy
import com.hazelcast.config.MapConfig
import com.hazelcast.config.SemaphoreConfig
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableCaching
class HazelcastConfig {
    @Bean
    fun hzConfig() = Config("instance").apply {
        setProperty("hazelcast.phone.home.enabled", "false")
        addMapConfig(MapConfig("map").apply {
            evictionPolicy = EvictionPolicy.LRU
            timeToLiveSeconds = 240
        })
    }
}
