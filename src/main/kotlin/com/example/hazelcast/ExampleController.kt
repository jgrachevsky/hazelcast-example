/*
 *  Created by Yakov Grachevsky
 *          at 1/27/19
 * -------------------------------------
 *    Copyright (c) 2019, BD Innovations (https://bd-innovations.com/) All Rights Reserved 
 */

package com.example.hazelcast

import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.core.ISemaphore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController


@RestController
class ExampleController(val hazelcastInstance: HazelcastInstance) {

    companion object {
        private val log: Logger = LoggerFactory.getLogger(ExampleController::class.java)
    }

    @GetMapping("/test/{script}/{timeout}")
    fun testHandler(@PathVariable script: String, @PathVariable timeout: Long): String {
        val distributedObject = hazelcastInstance.distributedObjects.find { it.name == script }
        val semaphore: ISemaphore
        if (distributedObject != null) {
            log.debug("Semaphore $script exists")
            semaphore = hazelcastInstance.getSemaphore(script)
        } else {
            log.debug("Semaphore $script does not exist")
            semaphore = hazelcastInstance.getSemaphore(script)
            log.debug("Semaphore $script created")
            semaphore.init(3)
        }
        return if (acquire(semaphore, script, timeout)) "OK" else "NOT PERMITTED"
    }

    private fun acquire(semaphore: ISemaphore, name: String, timeout: Long): Boolean {
        return if (semaphore.tryAcquire()) {
            log.debug("Semaphore $name acquired")
            GlobalScope.launch {
                log.debug("delay started")
                try {
                    delay(timeout)
                } finally {
                    semaphore.release()
                    log.debug("Semaphore $name released")
                }
                log.debug("delay ended")
            }
            true
        } else {
            log.debug("Semaphore $name limit achieved")
            false
        }
    }
}